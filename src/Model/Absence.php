<?php

namespace App\Model;

use DateTime;

Class Absence{
    private Datetime $dtBegin;
    private Datetime $dtEnd;

    /**
     * @return DateTime
     */
    public function getDtBegin(): DateTime
    {
        return $this->dtBegin;
    }

    /**
     * @param DateTime $dtBegin
     */
    public function setDtBegin(DateTime $dtBegin): void
    {
        $this->dtBegin = $dtBegin;
    }

    /**
     * @return DateTime
     */
    public function getDtEnd(): DateTime
    {
        return $this->dtEnd;
    }

    /**
     * @param DateTime $dtEnd
     */
    public function setDtEnd(DateTime $dtEnd): void
    {
        $this->dtEnd = $dtEnd;
    }
}
