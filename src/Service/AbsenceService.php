<?php
namespace App\Service;

use App\Model\Absence;
use DateTime;

Class AbsenceService
{
    private string $modifierBeginDT = 'first day of this month 00:00:00';
    private string $modifierLastDT = 'first day of next month 23:59:59';
    public DateTime $beginDay;
    public DateTime $lastDay;

    public function __construct() {
        // pay days definitions
        $this->beginDay = new DateTime($this->modifierBeginDT);
        $this->lastDay = new DateTime($this->modifierLastDT);
    }

    /**
     * Calculate if leaving period must be took into account for the current month
     * @param Absence $absence
     * @return bool
     */
    public function isInclusDansPeriode(Absence $absence): bool
    {
        // only two cases must be skipped
        return ! (
            ($absence->getDtBegin() < $this->beginDay && $absence->getDtEnd() < $this->beginDay)
            ||
            ($absence->getDtBegin() > $this->lastDay && $absence->getDtEnd() > $this->lastDay)
        );
    }

    /**
     * @param string $modifierBeginDT
     */
    public function setModifierBeginDT(string $modifierBeginDT): void
    {
        $this->modifierBeginDT = $modifierBeginDT;
    }

    /**
     * @param string $modifierLastDT
     */
    public function setModifierLastDT(string $modifierLastDT): void
    {
        $this->modifierLastDT = $modifierLastDT;
    }

}