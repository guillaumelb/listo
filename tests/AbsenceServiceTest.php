<?php

namespace App\Service;

use App\Model\Absence;
use DateTime;
use PHPUnit\Framework\TestCase;

class AbsenceServiceTest extends TestCase
{
    public function testIsInclusDansPeriode()
    {
        $srv = new AbsenceService();

        // one-day off + impossible human date
        $dayOff = new DateTime('1970-01-01');
        $test = 'oneDayOff';
        $$test = new Absence();
        $$test->setDtBegin($dayOff);
        $$test->setDtEnd($dayOff);
        self::assertEquals(
            false,
            $srv->isInclusDansPeriode($$test),
            $test . ': ' . $dayOff->format('Y-m-d H:i:s') . ' not part of current pay month'
        );

        // days before current pay month period
        $test = 'leaveBefore';
        $t1 = new DateTime('-50 day');
        $t2 = new DateTime('-52 day');
        $$test = new Absence();
        $$test->setDtBegin($t1);
        $$test->setDtEnd($t2);
        self::assertEquals(
            false,
            $srv->isInclusDansPeriode($$test),
            $test .': The period from ' . $t1->format('d-m-Y H:i:s') . ' to ' . $t2->format('d-m-Y H:i:s') . ' is part of current pay month'
        );

        // days after current pay month
        $test = 'notLeftYet';
        $t1 = new DateTime('+50 day');
        $t2 = new DateTime('+55 day');
        $$test = new Absence();
        $$test->setDtBegin($t1);
        $$test->setDtEnd($t2);
        self::assertEquals(
            false,
            $srv->isInclusDansPeriode($$test),
            $test .': The period from ' . $t1->format('d-m-Y H:i:s') . ' to ' . $t2->format('d-m-Y H:i:s') . ' is part of current pay month'
        );

        // days after current pay month
        $test = 'leftOneSecondBeforeEndOfPayMonth';
        $t1 = new DateTime('first day of next month 23:59:59');
        $t2 = new DateTime('+365 day');
        $$test = new Absence();
        $$test->setDtBegin($t1);
        $$test->setDtEnd($t2);
        self::assertEquals(
            true,
            $srv->isInclusDansPeriode($$test),
            $test .': The period from ' . $t1->format('d-m-Y H:i:s') . ' to ' . $t2->format('d-m-Y H:i:s') . ' is part of current pay month'
        );

        // days after current pay month
        $test = 'leftAtEndOfPayMonth';
        $t1 = new DateTime('first day of next month 00:00:00');
        echo $t1->format('Y-m-d H:i:s');
        $t2 = new DateTime('+365 day');
        $$test = new Absence();
        $$test->setDtBegin($t1);
        $$test->setDtEnd($t2);
        self::assertEquals(
            false,
            $srv->isInclusDansPeriode($$test),
            $test .': The period from ' . $t1->format('d-m-Y H:i:s') . ' to ' . $t2->format('d-m-Y H:i:s') . ' is part of current pay month'
        );

    }
}
